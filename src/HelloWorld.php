<?php

/**
 * Created by PhpStorm.
 * User: zgj12306
 * Email: zgj12306@163.com
 * Date: 2020/3/19
 * Time: 4:52 PM
 */

namespace HelloWorld;

class HelloWorld
{
    // 作者
    protected $author;

    /**
     * HelloWorld constructor.
     * @param string $author
     */
    public function __construct($author = 'zgj')
    {
        $this->author = $author;
    }

    /**
     * 执行方法
     * @return string
     */
    public function info()
    {
        $info = "Hello World ! \n";
        $info .= "\t--Power By ";
        $info .= $this->author . "\n";
        return $info;
    }
}